#!/home/michael/anaconda2/bin/python

from __future__ import print_function

import time
from datetime import date
import sys
import os

today = date.today()
today_string = (str(today.month)+
            '.'+str(today.day))
repo_path = '/home/michael/ctd/2016'
today_path = repo_path+today_string+'.md'


def printToday():
    if today_string+'.md' in os.listdir(repo_path):
        with open(today_path,'r+') as f:
            for line in f:
                print(line.strip())
    else: 
        print('Not present')


printToday() 