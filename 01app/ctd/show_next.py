#!/usr/bin/env python
import setup, util
import subprocess
import os
import glob
from datetime import date, timedelta

def print_next(offset=0,num_tasks=setup.next_items_count):
    util.set_context(offset)
    path_to_day = util.get_list_path(offset)
    # = max(glob.iglob(setup.repo_location+'*.*.md'), key=os.path.getctime)

    print('Showing next tasks from '+
            path_to_day+':')

    task_lines = []
    with open(path_to_day,'r') as f:
        task_lines = f.readlines()

    count = 1
    for line in task_lines:
        if util.has_task(line,unmarked=True):
            print line.strip()
            count += 1
            if count > num_tasks:
                break

