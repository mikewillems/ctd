import setup
import subprocess
from subprocess import *
import os
from datetime import date, timedelta


##################################
# DATETIME & FILE UTILS
##################################

template_path = setup.repo_location+'0.template.md'

today = date.today()

# Date format set here: 'M.D.md' for now.
# See 'date' python module to make other formats,
# then change list_fname_format in setup.py.
def get_list_fname(context=0,offset=0):
    if type(context) == type(today):
        context = context.day-today.day
    day = today + timedelta(days=context+offset)
    return setup.list_fname_format.format(dt=day)

def get_list_path(context=0,offset=0):
    return setup.repo_location+get_list_fname(context,offset)

def get_day(days_offset=0):
    return today+timedelta(days=days_offset)


#################################
# SUMMARIZATION UTILS
#################################

# Returns true if has both numeral and checkbox,
# and is not commented / a markdown heading.
def has_task(s,unmarked=False):
    s0 = s.split('.',1)
    try:
        int(s0[0])
    except:
        return False
    if unmarked:
        try:
            if not '[ ]' in s0[1][:5]:
                return False
        except:
            return False
    else:
        try:
            s1 = s0[1][:5].split('[')
            if not s1[1][1] == ']':
                return False
        except:
            return False
    return True

#################################
# FORMATTING & PARSING UTILS
#################################

checkbox = lambda(char):' ['+str(char)+'] '
task_line = (lambda task,num:
    str(num)+'. [ ] '+task)

# Parses a string into array of included numbers.
# Format can be: '1','1,3', '2-4', '2,3,7-9,5,13-17'
def str_extract_range(s,num_items):

    items = []

    if type(s) == type(0):
        return [s]
    else:
        s = str(s)

    if 'a' in s :
        return [i for i in range(1,1+num_items)]

    groups = []

    try:
        groups = [sub.split('-') for sub in s.split(',')]
        # [['1'],['2'],['5','7'],[''],['','12'],
        # ['','','']

        for n in groups:
            if len(n)==1:
                t = int(n[0])
                if t>0 and t<=num_items:
                    items.append(int(n[0]))
                else:
                    raise ValueError('out of range:\n'+str(n))
            elif len(n)==2:
                if n[0]=='':
                    n[0]=1
                elif n[1]=='':
                    n[1]=num_items
                for t in range(int(n[0]),int(n[1])+1):
                    items.append(t)
            else:
                raise ValueError('invalid argument:\n'+str(n))
    except Exception as e:
        print('Problem parsing due to '+format(e))
        print('Empty range returned.')

    return list(set(items))


#################################
# FILE I/O AND STORAGE
#################################

def readlines(filepath):
    with open(filepath,'r') as f:
        return f.readlines()

def overwritelines(filepath,lines):
    with open(filepath,'w') as f:
        f.write(''.join(lines))


############################
# STATE VARIABLES:
############################

# Stores the day offset most recently viewed:
def get_context():
    try:
        with open(setup.bin_location+'.context','r') as f:
            return int(f.readlines()[0].strip())
    except IOError:
        return 0

def set_context(offset):
    context = offset
    with open(setup.bin_location+'.context','w') as f:
        f.write(str(context))

context = get_context()


############################
# PROCESS UTILS:
############################


# Takes a single command or a list of
# separate commands and runs.
def run(command,
        working_dir=setup.repo_location,
        verbose=False):

    if not type(command) == type([]):
        command = [s for s in str(command).
                split(' ') if len(s)>0]

    for c in command:
        if(verbose):
            run_verbose(c,working_dir)
        else:
            run_quiet(c,working_dir)

def run_verbose(com,loc):
    subprocess.Popen(com,cwd=loc)
    return

def run_quiet(com,loc):
    subprocess.Popen(com,
            cwd=loc,
            stdin=PIPE,
            stdout=PIPE)
    return

