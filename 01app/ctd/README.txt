#######################################
# CtD Desktop App
# Version: 0.1
# Author: Michael Willems
# Updated: 16 Aug, 2016
#######################################

Welcome to CtD (Commit the Day). This is a 
workflow and toolset for collaborating on 
task management and accountability using git.

TO GET STARTED *****************
1) download the CtD python package
2) from this directory, run './setup'
3) if this does not work, add write permissions
   to this file with 'chmod +x setup' and repeat
   step 2
4) from the setup wizard, enter the local and
   remote repository paths and the desired 
   location for the ctd script files. Tip: to 
   avoid issues,place these files in a location
   that doesn't require sudo access.
5) check the user guide at any time with
   'ctd --guide' or use 'ctd --help' for a 
   quick reference.

