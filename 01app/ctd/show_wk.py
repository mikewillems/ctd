import setup, copy_list, util
from datetime import date, timedelta
import time
import os
import subprocess

def print_day(offset=0,edit=False):
    util.set_context(offset)

    day = util.today + timedelta(days=offset)
    day_path = util.get_list_path(day)
    prev_day_path = util.get_list_path(day,-1)

    if os.path.isfile(day_path):
        with open(day_path,'r+') as f:
            for line in f:
                print(line.strip())
        if edit:
            edit=raw_input("Edit list?")
            if(not 'n' in str(edit).lower()):
                subprocess.call([setup.editor_command,day_path])
    else:
        resp = str(raw_input('List not present. Create?\n'
                +"'y' = prev day\n"
                +"[#] = # days before current context\n"
                +"'a' = abbrev template\n"
                +"'f' = full template\n"))
        if 'y' in resp:
            print("Creating "+day_path+" from "+prev_day_path)
            copy_list.copy_to_day(offset)
            print_day(offset,edit=True)
        elif 'a' in resp:
            copy_list.copy_to_day(offset,source_path
                    = setup.path_abbrev_template)
            print_day(offset,edit=True)
        elif 'f' in resp:
            copy_list.copy_to_day(offset,source_path
                    = setup.path_full_template)
            print_day(offset,edit=True)
        else:
            try:
                source_offset = int(resp)
                copy_list.copy_to_day(offset,source_offset)
            except:
                return

