import setup
import util
from datetime import datetime
from datetime import date

today = date.today()
now = datetime.now()

command_1 = 'git add'

command_2 = 'git commit -m'

command_3 = 'git push'

def get_status_str(offset=0):
    checked = 0
    total = 0
    with open(util.get_list_path
            (today,offset),'r') as f:
        lines = f.readlines()
        for line in lines:
            if '[ ]' in line:
                total +=1
            elif '[x]' in line:
                checked +=1
                total+=1
            # TODO check / label canceled sep.
            # label and % failure also.
            elif '[-]' in line:
                total+=1
    denom = total
    if total == 0:
        denom = 1
    return (str(checked)+'/'+str(denom)+', '+
        str(int(checked*100.0/denom))+'% complete.')

def commit(offset=0):
    if type(offset) == type(''):
        for day in offset:
            commit(day)

    to_add = util.get_list_path(today,offset)

    # TODO - should be stored in setup and
    # differentiate message between init,
    # update, and final commit:
    # init_message = setup.first_commit_message
    # final_message = setup.last_commit_message
    message = ('"'+str(now.year)+'.'+
            str(now.month)+'.'+
            str(now.day)+' '+
            str(now.hour)+':'+
            str(now.minute)+' -> '+
            get_status_str(offset)+'"')

    add_command = ['git','add',to_add]
    commit_command = ['git','commit','-m',
            message]
    push_command = ['git','push']

    util.run([add_command,
              commit_command,
              push_command])

def status():
    util.run([['git', 'status']],verbose=True)
