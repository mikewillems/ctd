import subprocess,os
import util,setup

def copy_to_day(
        dest_offset,
        rel_source_offset=-1,
        source_path="",
        items=0):

    if len(source_path)==0:
        source_path = util.get_list_path(util.get_day(dest_offset+rel_source_offset))
    dest_day = util.get_day(dest_offset)
    dest_path = util.get_list_path(dest_day)

    if os.path.isfile(source_path):
        subprocess.call(['cp',source_path,dest_path])
        dest_list_header = setup.list_header_format.format(dt=dest_day)

        lines = []

        with open(dest_path,'r') as f:
            lines = f.readlines()
        if items<1:items=len(lines)
        else:items+=1
        with open(dest_path,'w') as f:
            f.write(dest_list_header+'\n')
            for line in lines[1:items]:
                f.write(uncheck_box(line))
    else:
        print('Cannot copy '+source_path+'; file does not exist.')

def uncheck_box(line):
    sp1 = line.split('[',1)
    if (not line[0] == '#' and
        len(sp1)==2 and len(sp1[1])>3
        and sp1[1][1]==']'):
        line = sp1[0]+'[ ]'+sp1[1][2:]
    return line
