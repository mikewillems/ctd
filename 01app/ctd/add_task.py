import util
from collections import defaultdict

checkbox = lambda(char):' ['+str(char)+'] '

# add task to end of list
def add(task,offset=0):

    task = ' '.join(task)

    path_to_day = util.get_list_path(util.today,offset)

    # Get lines from list file
    task_lines = util.readlines(path_to_day)

    # Find num tasks and loc of last task
    task_index = get_task_locations(task_lines)
    keys = sorted(task_index.keys())
    task_num = max(keys)+1

    # Insert line in list
    task_lines.insert(
        #task_index[max(keys)]+1,
        len(task_lines),
        util.task_line(task+'\n', task_num))

    # Write list of lines back to file:
    util.overwritelines(path_to_day,task_lines)

# TODO write script to move rest of tasks
# down and insert at proper task num
def insert_task(task,position,offset=0):
    raise NotImplementedError


# Returns dictionary mapping present task numbers
# to their line location.
def get_task_locations(task_lines):
    task_locations = defaultdict(int)
    for loc,line in enumerate(task_lines):
        try:
            sp = line.split('. [',1)
            if (len(sp)==2 and
                    len(sp[1])>3 and
                    sp[1][1]==']'):
                num = int(sp[0])
                task_locations[num] = loc
        except ValueError:
            continue
    return task_locations
