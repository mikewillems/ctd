try:
    from setuptools import setup
except Import Error:
    from distutils.core import setup

config = {
    'description': 'Commit the Day CLI',
    'author': 'Mike Willems',
    'url': 'http://mikewillems.com/ctd',
    'download_url': 'http://github.com/mikewillems/ctd',
    'author_email': 'ctd@mikewillems.com',
    'version': '0.1',
    'install_requires': ['nose'],
    'packages': ['ctd'],
    'scripts': [],
    'name': 'ctd'
    }

setup(**config)
