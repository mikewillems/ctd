#!python3
# v0.2, 5.10.2023
# This file arranges env variables and paths
# to allow ctd to work.


##########
# IMPORTS

from __future__ import print_function
import sys
import os
import subprocess


###########
# INTERFACE VARIABLES

# How many items to display at next call
next_items_count = 5

# What day (offset from today) to pull tasks from for do()
context = 0

###########
# USER VARIABLES

# Where the program should is stored - modify by moving files on OS:
bin_location = os.getcwd()
user_home = os.path.expanduser('~')

# Defaults to ubuntu (~/.profile), could be ~/.bash_profile for other Linux versions
bash_profile_path = user_home + '/.zshrc'

# Repo location - can be whatever user chooses.
repo_location = user_home + '/ctd/2023/'

# Date format. See 'date' python module for other formats.
list_fname_format = '{dt.month}.{dt.day}.md' # In strftime format, current:'m.d.md'
list_header_format = '# {dt:%a} {dt.year}.{dt.month}.{dt.day}'
path_full_template = bin_location+'full_template.md'
path_abbrev_template = bin_location+'abbrev_template.md'
base_list_separator = "----------------------------"

# Editor - can be a path or a shell command:
# editor_command = '/usr/bin/subl'
editor_command = '/usr/bin/vim'

##########
# SYSTEM VARIABLES

system_path = os.environ['PATH']

# setup function:
def setup():

    print('Welcome to Commit the Day!');

    # Check if save directory exists, create if not:
    if not os.path.exists(repo_location):
        print("The specified repository location doesn't exist yet, creating it now at "+repo_location+"...")
        process = subprocess.Popen(["mkdir","-p",repo_location], cwd=bin_location)
        process.wait()

    bash_profile_appendix = (
        ['# Add CTD bin directory to PATH:',
        'ctd_path="'+bin_location+'"',
        'case ":$PATH:" in',
        '*":ctd_path:"*) :;; # already there',
        '# else/default clause >> new entry:',
        '*) PATH="$PATH:$ctd_path";;',
        'esac'])

    # Check if in bash profile
    with open(bash_profile_path,'a+') as f:
        bash_profile = f.read()
        if 'ctd_path' not in bash_profile:
            print('No ctd record found. Adding to bash profile.')
            for line in bash_profile_appendix:
                f.write(line+'\n')
        elif bin_location not in bash_profile:
            print('It seems that CtD was previously installed to another location. To continue with this setup, please remove the old setup from the path by modifying the $PATH variable and/or changing the bash profile at:'+ bash_profile_path)
        else:
            print('This CtD was already added to the path.')

        # Now prompt re-login if needed.
        if bin_location not in system_path:
            logout = raw_input('This process may not work in other bash windows until you re-login to the system. Would you like to logout now?\n')
            if('y' in logout or 'Y' in logout):
                logout = raw_input('Please ensure all work is saved. Confirm logout?\n')
                if('y' in logout or 'Y' in logout):
                    subprocess.call(['/usr/bin/gnome-session-quit','--no-prompt'])
            print("Okay. Please re-login as soon as possible.")
                # For reference, here is your path:\n"+os.environ.get("PATH"))

args = sys.argv
if '--run' in args:
   setup()
# instead, run without the --run flag:
# setup()

##########
# TODO
# Consider using os.chmod to check for being executable
