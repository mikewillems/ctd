class DailyList:
    """
    DailyList is a class specifically for storing immediate Tasks. It is effectively
    a workspace for bringing tasks onto one's "plate" for the day ("biting" them off,
    in CtD terminology). It stores the day's tasks while CtD is working with them
    and loads / conveys UI preferences about the behavior of list actions.

    As a "dumb" class, it gets recreated each time a daily list is loaded by parsing
    the markdown file.
    """
    def __init__(self,date,existing_list=None,new_list_metadata=None,list_preferences=None) -> None:
        self.date = date
        if existing_list:
            self.tasks = existing_list.tasks
            if existing_list.list_preferences and not list_preferences:
                self.list_preferences = existing_list.list_preferences
        else:
            self.tasks = []
            self.list_preferences = list_preferences
        self.meta = new_list_metadata
    
    def __str__(self) -> str:
        return f"{self.date} {self.tasks}"