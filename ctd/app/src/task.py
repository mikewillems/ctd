from src import index_actions


class Task:
    """
    Task class is for serializing, deserializing, and displaying the information about a task.
    It doesn't "do" anything and merely gets used and passed around between lists.
    It is meant to be as minimalist as possible to make serialization and deserialization
    as fast as possible, as each task object in a given list is read from disk at each ctd 
    function call involving that list and written each time any item in the list is modified.
    """
    def __init__(self, text, uid, mark=" ", id="", external_id="", additional_data=None, tags=None) -> None:
        self.text = text
        self.mark = mark
        self.UID = uid
        self.external_id = external_id
        self.additional_data = additional_data
        self.tags = tags
    def __str__(self) -> str:
        # Formats: 
        # [ ] text
        # [-] <external_id> text
        # [/] … text (assuming using `…` as indicator for additional data)
        # [x] … <A123> text
        # [x] … <A123> text #tag1 #tag2 - tags are embedded in the text and
        #   extracted at runtime when reading from disk.
        return "[{mark}] {subtask_indicator}{external_id}{text}{tag_list}".format(
            mark=self.mark[0] if self.mark and len(self.mark)>0 else " ",
            subtask_indicator=config.task_has_subtasks_indicator if self.additional_data else "",
            external_id=f"<{self.external_id}> " if len(self.external_id)>0 else "",
            text=" " + self.text if self.text and len(self.text)>0 else "",
            tag_list=" #" + " #".join(self.tags) if self.tags and len(self.tags)>0 else "",
        )