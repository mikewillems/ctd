def task_create(new_task):
    # create a new task object
    # use index_actions to add task to index
    pass

def task_delete_from_index(task_id):
    # use index_actions to get task object
    # remove task object from index and all lists
    pass

def task_modify_lists(task_id, list_ids_to_remove_from, list_ids_to_add_to):
    # use index_actions to get task object
    # copy task object to another list and delete from current list
    pass

def task_add_task_data_directory(task_id, custom_data_path=None):
    # create and link a directory for storing additional task data
    # use index_actions to update index if applicable
    # add directory field to task object
    pass



