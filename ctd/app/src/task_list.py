class TaskList():
    def __init__(self):
        self.tasks = {}
    
    def __len__(self):
        return len(self.tasks)

    def __iter__(self):
        return iter(self.tasks)

    def __getitem__(self, task_key):
        return self.tasks[task_key]

    def __setitem__(self, task_key, value):
        self.tasks[task_key] = value

    def __delitem__(self, task_key):
        del self.tasks[task_key]

    def __contains__(self, item):
        return item in self.tasks

    def __str__(self):
        return str(self.tasks)

    def __repr__(self):
        return repr(self.tasks)

    def _get_next_task_number(self):
        return max(filter(lambda key: type(key) is int, self.tasks.keys())) + 1 if len(self.tasks) > 0 else 1

    def add_task(self, task):
        self.__setitem__(self._get_next_task_number(), task)

    def remove_task(self, task_key):
        self.__delitem__(task_key)

    def get_tasks(self):
        return self.tasks

    def get_task(self, task_key):
        return self.tasks[task_key]
    
    def mark_task(self, task_key, mark):
        self.tasks[task_key].mark = mark
    

