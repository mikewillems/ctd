"""
File: file_io.py
Author: Mike
Description: This file contains functions related to file input/output operations.
"""

from pathlib import Path as P
import sys

DIRECTORY_PATH_STATUS_CODE = ["Created directory", "Directory exists, used existing", "Directory path is a file, did not create", "Parent directory does not exist, did not create"]
FILE_PATH_STATUS_CODE = ["Created file", "File exists, did not create", "File exists as a directory, did not create", "Parent directory does not exist, did not create"]
enumerate = lambda x: list(zip(range(len(x)), x))
enumerated_directory_status = enumerate(DIRECTORY_PATH_STATUS_CODE)
enumerated_file_status = enumerate(FILE_PATH_STATUS_CODE)

def standardize_dir_location(location="./"):
    location = str(location).strip()
    if "~" in location:
        # print("'~' character detected in filepath. Replacing with home directory.")
        location = location.replace("~", str(P.home()))
    if len(location) < 1:
        location = "./"
    elif location[0] != "/":
        location = "./"+location
    if location[-1] != "/":
        location = location+"/"
    return location

def touch_file(name, location="./"):
    location = str(location).strip()
    return_code = None
    location = standardize_dir_location(location)
    split_location = location.split("/")

    parent_dir = P("/".join(split_location[:-2]))
    if not parent_dir.exists():
        # print("Base directory does not exist: "+str(parent_dir)+" Aborting.")
        return (DIRECTORY_PATH_STATUS_CODE[3], FILE_PATH_STATUS_CODE[3])
    elif not P(location).exists():
        P(location).mkdir()
        # print("Created directory: "+split_location[-2] + " in: "+str(P("/".join(split_location[:-2])))))
        return_code = (DIRECTORY_PATH_STATUS_CODE[0], None)
    else:
        # print("Directory already exists: "+location)
        if P(location).is_file():
            return (DIRECTORY_PATH_STATUS_CODE[2], FILE_PATH_STATUS_CODE[2])
        else:
            return_code = (DIRECTORY_PATH_STATUS_CODE[1], None)
    qualified_name = location+name
    file_path = P(qualified_name)
    if not file_path.exists():
        file_path.touch()
        # print("Created file: "+qualified_name)
        return_code = (return_code[0], FILE_PATH_STATUS_CODE[0])
    else:
        # print("File or directory already exists: "+qualified_name)
        if file_path.is_dir():
            return_code = (return_code[0], FILE_PATH_STATUS_CODE[2])
        else:
            return_code = (return_code[0], FILE_PATH_STATUS_CODE[1])

    return return_code

def read_file(name, location="./"):
    location = standardize_dir_location(location)
    qualified_name = location+name
    file_path = P(qualified_name)
    if not file_path.exists():
        # print("File does not exist: "+qualified_name)
        return None
    elif file_path.is_dir():
        # print("File path is a directory: "+qualified_name)
        return None
    else:
        with open(qualified_name, "r") as file:
            return file.read()

# Note: This function will only work the file if it already exists.
def write_file(name, content, append=False, location="./"):
    location = standardize_dir_location(location)
    qualified_name = location+name
    file_path = P(qualified_name)
    if not file_path.exists():
        # print("File does not exist: "+qualified_name)
        raise FileNotFoundError("File does not exist: "+qualified_name)
    elif file_path.is_dir():
        # print("File path is a directory: "+qualified_name)
        raise IsADirectoryError("File path is a directory: "+qualified_name)
    else:
        if append:
            with open(qualified_name, "a") as file:
                file.write("\n"+content)
        else:
            with open(qualified_name, "w") as file:
                file.write(content)

def list_directory(location="./"):
    location = standardize_dir_location(location)
    return P(location).iterdir()

def create_directory(location="./"):
    location = standardize_dir_location(location)
    return_code = None
    location = standardize_dir_location(location)
    split_location = location.split("/")

    parent_dir = P("/".join(split_location[:-2]))
    if not parent_dir.exists():
        # print("Base directory does not exist: "+str(P("/".join(split_location[:-2]))))
        return DIRECTORY_PATH_STATUS_CODE[3]
    elif not P(location).exists():
        P(location).mkdir()
        # print("Created directory: "+split_location[-2] + " in: "+str(P("/".join(split_location[:-2]))))
        return_code = DIRECTORY_PATH_STATUS_CODE[0]
    else:
        # print("Directory already exists: "+location)
        return_code = DIRECTORY_PATH_STATUS_CODE[1]
    return return_code
