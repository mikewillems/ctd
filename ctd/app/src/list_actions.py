def list_add_task(list_id, task_to_add):
    # use index_actions to check for task UID
    # create new task object using task_actions if UID doesn't exist, 
    # otherwise load the existing task object
    # add task object to list
    pass

def list_remove_task(list_id, task_id):
    # use index_actions to get task object
    # remove task object from list
    pass

def list_mark_task(list_id, task_id, task_mark):
    # use index_actions to get task object
    # mark the task object using task_actions
    pass

def list_edit_task_text(list_id, updated_task):
    # use index_actions to get task object
    # edit task object using task_actions
    pass

def list_check_data_directory(list_id):
    # check if current list has a directory for storing additional 
    # task data, return a directory listing if it does
    pass

def list_add_data_directory(list_id, custom_data_path=None):
    # create and link a directory for storing additional task data
    # pertaining to the entire list, rather than a single task
    # use index_actions to update index if applicable
    pass
