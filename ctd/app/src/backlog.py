class Backlog:
    def __init__(self, backlog_id, name, description, external_id, git_info, ownership_info):
        self.backlog_id = backlog_id
        self.name = name
        self.description = description
        self.status = status
        self.priority = priority
        self.project_id = project_id

    def __str__(self):
        return f"{self.backlog_id} {self.name} {self.description} {self.status} {self.priority} {self.project_id}"

    def __repr__(self):
        return f"{self.backlog_id} {self.name} {self.description} {self.status} {self.priority} {self.project_id}"