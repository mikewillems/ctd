from src import daily_list_actions
from src import daily_list

def test_add_task():
    # start with list of 0 tasks
    list = daily_list.DailyList()
    assert len(list.tasks) == 0
    # add a task
    daily_list_actions.add_task(list)   
    # assert list has 1 task
    # assert task has correct UID
    # assert task has correct text
    # assert task has correct mark
    # assert task has correct external_id
    # assert task has correct additional_data
    assert False

def test_delete_task():
    assert False

def test_mark_task():
    assert False

def test_unmark_task():
    assert False

def test_edit_task_text():
    assert False

def test_move_task():
    assert False

def test_add_additional_data():
    assert False

# Path: ctd/ctd/test/test_daily_list_actions.py