import task

from random import randint

def _random_id():
    return randint(1, 1e20)

id = _random_id()
def test_task_init_text_only():
    t = task.Task("test", id)
    assert t.text == "test"
    assert t.mark == " "
    assert t.UID == id
    assert t.external_id == ""
    assert t.additional_data == None
    assert str(t) == "[ ] test"

id = _random_id()
def test_task_init_with_mark():
    t = task.Task("test", id, mark="x")
    assert t.text == "test"
    assert t.mark == "x"
    assert t.UID == id
    assert t.external_id == ""
    assert t.additional_data == None
    assert str(t) == "[x] test"

id = _random_id()
def test_task_init_with_external_id():
    t = task.Task("test", id, external_id="A1234")
    assert t.text == "test"
    assert t.mark == " "
    assert t.UID == id
    assert t.external_id == "A1234"
    assert t.additional_data == None
    assert str(t) == "[ ] <A1234> test"

id = _random_id()
def test_task_init_with_additional_data():
    t = task.Task("test", id, additional_data="/path/to/folder")
    assert t.text == "test"
    assert t.mark == " "
    assert t.UID == id
    assert t.external_id == ""
    assert t.additional_data == "/path/to/folder"
    assert str(t) == f"[ ] {config.task_has_additional_data_indicator} test"

id = _random_id()
def test_task_init_with_all():
    t = task.Task("test", id, mark="x", external_id="A1234", additional_data="/path/to/folder")
    assert t.text == "test"
    assert t.mark == "x"
    assert t.UID == id
    assert t.external_id == "A1234"
    assert t.additional_data == "/path/to/folder"
    assert str(t) == f"[x] {config.task_has_additional_data_indicator} <A1234> test"
