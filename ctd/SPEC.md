<!--Here's some front matter / metadata-->
<details> <summary><b>My section header in bold</b></summary> this is foldable </details>

# Commit the Day Specification

## Major UI Functions

1. Read, create, modify, and write daily markdown TODO lists obeying the following format: 
```
# Ddd yyyy.mm.dd

1. [ ] Task 1
2. [ ] Task 2
3. [ ] Task 3
```
2. Mark the boxes in them with the desired characters and unmark them.
3. Allow similar creation of "project" lists and backlogs, from which daily lists can be assembled by "biting off" tasks. Do not mark the project task complete or remove it from the project list until it has been completed in the daily list, but do indicate that it has been started / bitten off. Allow both copying and moving of project list tasks to other project lists.
    1. The default project list should be weekly.
    2. Enable a collected view of tasks from all projects.
4. Allow renumbering and reordering of tasks, but do not require it - i.e. allow tasks to exist out of order and be associated with a particular number, so that users don't have to relearn the number of the tasks after every ordering. Only rewrite the remaining task numbers when inserting a task into the list, and only renumber as many tasks as necessary to allow it to fit. Issue a confirmation dialog first.
5. Allow for template lists so that the days tasks may be pre-filled. Eventually, this should be day-of-week sensitive.
6. Allow for deferring uncompleted tasks to the next day, and in general for carrying those tasks over. 
7. Provide statistics on a daily and project-ly basis for task completion.
8. Provide a quick view that shows the next N tasks in the list without showing the whole thing, similar to `head -[N]`, by default 3.
9. Allow quick viewing other days' tasks, especially for "tomorrow".
10. Allow quick viewing of all completed tasks.
11. Enable committing the day's changes (with optional commit message prompt) and pushing to the remote git repository. If desired by the user, do this each time the list is changed and either way, flag the daily commit with a special commit tag.

## Implementation

1. Use git repositories for the backend.
2. Minimize activation energy / effort for using the system: avoid complexity in the UI wherever possible. Simpler is better than featureful, when it comes to a tool for staying undistracted.
3. CtD is a CLI-first application.
4. The CLI should work for windows, macos, and linux computers, and should be able to be installed with a simple setup script, assuming the system has some Python 3 version installed. A bundled distribution including a Python runtime should be available.
5. CtD is coded in Python. Possible add-ons include an Electron desktop / web client and a mobile app in Dart or React Native.