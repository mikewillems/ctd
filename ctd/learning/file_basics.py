#!/usr/bin/env python3

from pathlib import Path as P
import sys

def create_file(name, location="./"):
    location = str(location).strip()

    # standardize location string:
    if "~" in location:
        print("'~' character detected in filepath. Replacing with home directory.")
        location = location.replace("~", str(P.home()))
    if len(location) < 1:
        location = "./"
    elif location[0] != "/":
        location = "./"+location
    if location[-1] != "/":
        location = location+"/"
    split_location = location.split("/")

    parent_dir = P("/".join(split_location[:-2]))
    if not parent_dir.exists():
        print("Base directory does not exist: "+str(parent_dir)+" Aborting.")
        return
    elif not P(location).exists():
        parent_dir = P("/".join(split_location[:-2]))
        P(location).mkdir()
        print("Created directory: "+split_location[-2] + " in: "+str(parent_dir))
    else:
        print("Directory already exists: "+location)
    qualified_name = location+name
    print("Qualified name: "+qualified_name)
    file_path = P(qualified_name)
    if not file_path.exists():
        file_path.touch()
        print("Created file: "+qualified_name)
    else:
        print("File or directory already exists: "+qualified_name)


def main():
    new_file_name = sys.argv[1] if len(sys.argv) > 1 else input("Enter a file name to touch: ")
    dir_name = sys.argv[2] if len(sys.argv) > 2 else input("Enter a directory in which to create the file. Can be existing or new (defaults to cwd): ")
    create_file(new_file_name, dir_name)
    # print("Created file: "+new_file_name+" in directory: "+dir_name)

main()