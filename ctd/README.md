
# What Is It?

Open source projects have a really bad tendency to not say what they actually do. Let's fix that.

Commit the Day is a task management and sharing scheme for computer users who like git and accountability. Using a git repo, you can securely store your tasks, access them from anywhere, and share them with a team, making use of all of the standard features of a git repository.

By default, CtD is a command line interface tool - similar to TaskWarrior or todo.txt, but written entirely in python and using git and local markdown files to store things. It is extensible to GUI applications, e.g. for mobile, and for syncing with project management frameworks, such as Jira. Such extensions are planned for future development, but importantly, using CtD doesn't have to take you away from the terminal to any other context. You can check and manage your plan with minimal friction and no distractions.

The philosophy behind this project is that there is value in making a commit that can extend beyond just lines of code. You have things in your life that you want to get done. Just like in software, a daily commit shows you and others what you have accomplished for the day. The hope is that you can share a repo with accountability partners, and, if you like, use it for work as well. It loosely implements David Allen's "Getting Things Done" modality, in that you can create tasks for any and all things you come across, and use your day's list as a "bucket". This is especially supported by task templates for repetitive tasks that you want to appear every day, and by the ability to very easily move tasks into and out of backlogs.

This is the top level repo for all CtD versions.

# How it Works

CtD is designed to be extremely lightweight and minimally burdensome to use. The design decision was made, therefore, to carefully pollute the terminal namespace a bit for the sake of typing less during use. Most CLI tools that require ninja-level typing skills to use them with any efficiency, so CtD tries not to. Instead, common commands are available directly from the command line, and arguments parsed in accordingly.

When you issue any CtD command to your terminal, it runs a script that reads any necessary daily or project-specific task files, parses them into a Python data structure, makes any requested changes, and writes the file back out. Depending on your settings, this automatically updates any desired remote repos after each change or only when issusing a the `ctd` command to _commit_ the day.

## Installation

### Installing from Source

### Installing from Pip

### Installing Standalone (Without Installing Python)

## Setup

### Setup with Python

After installation, simply open a terminal in the `ctd` program root folder and issue the command `python setup`. You will be prompted to interactively modify the `config` file, which contains all user settings, but you can customize them at any time by modifying the file directly. Customizations will include operating system specifics, remote and local git repo paths (and keys if applicable), and sync behavior. Then you are ready to use CtD.

### Standalone Setup

TBD

## Basic Usage



## Command Reference

# Project History

Mike Willems wrote the first version of CtD in August 2016 to help him focus on projects. It was written in Python 2.7 and was a series of linked unix utilities that made changes directly to task files. He updated it to Python 3.10.6 in May of 2023, adding addtional functionality. Any feedback is appreciated!
