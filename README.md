# CareArcs README

## What Is It?

Ever get annoyed at how open source projects rarely actually say what they _are_?

CommitTheDay (CTD) is a task management and accountability scheme for people who like Git and Getting Things Done (GTD). It's a command line interface tool - similar to TaskWarrior or todo.txt, but written almost entirely in Python (with a bit of Bash) and uses Git and Markdown files to store things.

With CTD, you can securely store your tasks online, access them from anywhere, and share them with friends or a team. CTD provides a powerful and thoughtful and set of commands that maximize - and enforce - speed and simplicity with minimal overhead. Moreover, you can use of all of the standard features of Git, from anywhere and using any terminal you like.

## Design Philosophy

The main motivating philosophy behind this project has three tenets:

1. The value of making a commit can extend beyond just lines of code. Just like in software, a daily commit of what you want to get done and what you have gotten done in life lets you and others track your accomplishments in an accountable way.
2. An effective task management software must take almost no effort to use. It must rely on very few commands, get out of the way as easily as possible, and integrate well into an existing workflow with ZERO added clutter. No menu bar or persistent screen real estate. The point is to replace a separate physical TODO list, so it must be visible without finding a particular tab or window, and it must fully disappear and reappear at will without any consequence or appreciable load time to allow deep focus on each task.
3. It absolutely must tolerate compounding or stacking workload without ease of use being affected, even a little bit. It must be immediately usable at any time, without any barrier, no matter how much work has piled up, and no matter how long it has been neglected. It cannot stop functioning because you took a vacation or didn't have time to process your bucket and prioritize your backlog for 2 months. This is the only way it will remain useful for clearing up these common situations.

To accomplish these goals, CTD is designed as follows:

1. It reduces effort for Git use to the point of laziness. Completed tasks are shown first, Git commitment (and pushing if set up) is shortened to a single three-letter command with automatic message generation, and the user is rewarded with daily stats at this point. Any accountability partners can track the repo with whatever app they like and immediately see and comment on the update.
2. CTD includes only 20 very atomic commands, and most require at most one argument and less than 6 characters to type. The downside of this is polluting path, but the uniqueness of the command names, combined with the ability to alias them as desired, overcomes that difficulty.
3. CTD is task-focused, not project focused. While you can have however many projects you like as different backlogs, the action all happens at the daily list level, and a clogged backlog doesn't affect the usability of the daily manager at all, nor prevent the creation of additional or temporary backlogs. Those logs will stay on your system as is, in a human-readable directory and file format at an easily reachable location, until you're ready to deal with them. Every single day, you can start off with a fresh list and easily copy over any tasks you like from any other day and any backlog with a single command, on a strictly as-needed, intentional basis.

## Command Specification

For the latest version of CTD, the commands are as follows:

### Display Commands

1. //cbucket (clist -b / --bucket) : e
2. //ccal: vlcfner
3. cday: vlcfert
4. cdone
5. //cinfo
6. //clist: vcfner
7. cnext: l
8. //copen
9. //cstat
10. //cwk: l
11. cjournal: vrt

### Edit Commands

1. cadd: plcd
2. //ccopy: lcd
3. cdef
4. cdel: lcd
5. cdo: dl (cmod --mark / -m)
6. cins (cadd --position / -p)
7. cmod: lcd
8. //cmove: lcd (ccopy + cdel)
9. cord: li (i = ignore lists, a = altogether)
10. //ctag: lcd
11. cswap

### Management Commands

1. //csettings
2. //cshare
3. ctd

This is the top level repo for all Commit the Day versions.

cday

1. -p [n] lists prior n entries regardless of day (default 10)

### Flag Descriptions

1. -v --view, often takes argument [n] (default 10) to show how many if previous or upcoming
2. -u [n] specifies upcoming n timed entries (default 10)
3. -f name[,other_names] filters to only show entries from selected named calendars or lists
4. -n [name] creates new list, tag, or calendar
5. -c [calendar_name] list or calendar to new named calendar
6. -d [day_offset_num] specifies a day
7. -l [csv,list_names] shows only entries from list name(s) only