import util
from collections import defaultdict

checkbox = lambda(char):' ['+str(char)+'] '

# add task to end of list
def modify(task_num,task,offset=0):

    path_to_day = util.get_list_path(util.today,offset)

    # Get lines from list file
    task_lines = util.read_lines(path_to_day)

    # Find num tasks and loc of last task
    task_index = get_task_locations(task_lines)

    # task_num comes as single element list.
    # Get the integer:
    task_num = int(task_num[0])

    if not task_num in task_index.keys():
        print "Specified task does not exist."
        return

    else:
        task_line = task_lines[
                task_index[task_num]]

        if len(task) == 0 or len(task[0]) == 0:
            print "Task detail is currently:"
            print task_line.split('] ',1)[1]
            task = raw_input(
                "Enter the new task detail:\n")

        else:
            task = ' '.join(task)

        task_lines[task_index[task_num]]=(
            util.task_line(task+'\n', task_num))

        # Write list of lines back to file:
        util.overwrite_lines(path_to_day,
                task_lines)


# Returns dictionary mapping present task numbers
# to their line location.
def get_task_locations(task_lines):
    task_locations = defaultdict(int)
    for loc,line in enumerate(task_lines):
        try:
            sp = line.split('. [',1)
            if (len(sp)==2 and
                    len(sp[1])>3 and
                    sp[1][1]==']'):
                num = int(sp[0])
                task_locations[num] = loc
        except ValueError:
            continue
    return task_locations
