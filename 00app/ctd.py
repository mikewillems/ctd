import setup
import util
import datetime

now = datetime.datetime.now()

command_1 = 'git add'

command_2 = 'git commit -m'

command_3 = 'git push'


def process(offset,status,git_status):
    if status:
        print (get_status_str(offset))
    if git_status:
        call_git_status()
    if not status and not git_status:
        commit(offset)


def get_status_str(offset=0):
    checked = 0
    total = 0
    with open(util.get_list_path
            (offset),'r') as f:
        lines = f.readlines()
        for line in lines:
            if '[ ]' in line:
                total +=1
            elif '[x]' in line:
                checked +=1
                total+=1
            # TODO check / label canceled sep.
            # label and % failure also.
            elif '[-]' in line:
                total+=1
    denom = total
    if total == 0:
        denom = 1
    return (str(checked)+'/'+str(denom)+', '+
        str(int(checked*100.0/denom))+'% complete.')

def commit(offset=0):
    if type(offset) == type(''):
        for day in offset:
            commit(day)

    to_add = util.get_list_path(offset)

    # TODO - should be stored in setup and
    # differentiate message between init,
    # update, and final commit:
    # init_message = setup.first_commit_message
    # final_message = setup.last_commit_message
    context_date = (datetime.date.today()+
            datetime.timedelta(days=offset))
    # long-form commit message:
    # message = ('"'+str(context_date.year)+'.'+
    #         str(context_date.month)+'.'+
    #         str(context_date.day)+' update @'+
    #         str(now.hour)+':'+
    #         str(now.minute)+' -> '+
    #         get_status_str(offset)+'"')
    message = (get_status_str(offset)+'@'+
            str(now.hour)+':'+
            str(now.minute)
            )

    add_command = ['git','add',to_add]
    commit_command = ['git','commit','-m',
            message]
    push_command = ['git','push']

    util.run([add_command,
              commit_command,
              push_command])

def call_git_status():
    util.run([['git', 'status']],verbose=True)
