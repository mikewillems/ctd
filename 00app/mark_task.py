import util
from collections import defaultdict

# mark list of tasks from specified day with specified character.
def mark(todo='-1',offset=0,mark='x'):

    if len(str(mark)) == 0:
        mark = ' '
    else:
        mark = str(mark)[0]

    if mark == '/':
        mark = '~'

    # Get lines from list file
    path_to_day = util.get_list_path(util.today,offset)
    task_lines = []
    task_lines = util.read_lines(path_to_day)

    ########################################
    # Map each task # to corresponding line #
    # Define dictionary:
    # task_index[task_num]=[line_num,box,task]
    task_index = {}

        # ...by finding tasks in the lines
    for (line_num,line) in enumerate(task_lines):
        if '] ' in line[:9] and not '#' in line[:9]:
            spl1 = line.split('] ',1)
            spl2 = spl1[0].split('.',1)
            if len(spl1)+len(spl2)==4:
                task_num = spl2[0]
                # If a checkbox is commented, w/out num
                # would throw error - catch and move on
                try:
                    task_num = int(task_num)
                except ValueError:
                    continue
                box = spl2[1]+'] '
                # Finally, if all checks work, add to index.
                task = spl1[1]
                task_index[task_num]=[line_num,box,task]


    # Replace checkbox with proper one.

    if todo == '-1' or len(str(todo)) == 0: # Replace first unchecked task only
        for task_num in sorted(task_index.keys()):
            if task_index[task_num][1] == ' [ ] ':
                line = (str(task_num) + '.' +
                        util.checkbox(mark)+task)
                break
    else: # replacing list of checkboxes with given marker
        if len(mark)<1:
            mark = 'x'
        # If range = 'rest' or 'r', mark rest
        if 'r' in todo:
            todo = range(1,max(task_index)+1)
            todo = [i for i in todo if
                    i in task_index.keys() and
                    task_index[i][1] == ' [ ] ']
        else:
            todo = util.str_extract_range(todo,max(task_index))
        for task_num in todo:
            task_det = task_index[task_num]


             # Overwrite line in list:
            task_lines[task_det[0]] = (
                    str(task_num)+'.'+
                    util.checkbox(mark)+
                    task_det[2])

    # Write lines back to file:

    util.overwrite_lines(path_to_day,task_lines)
