#######################################
# CtD Desktop App
# Version: 0.1
# Author: Michael Willems
# Updated: 16 Aug, 2016
#######################################

Welcome to CtD (Commit the Day). This is a 
workflow and toolset for collaborating on 
task management and accountability using git.

TO GET STARTED *****************
1) Move this ctd package to some
   subdirectory of your user directory (~)
2) In setup.py, enter the local and remote 
   repository paths. Tip: to avoid issues,
   place these files in a location that 
   doesn't require super user access.
3) From this ctd directory, run './setup'
4) If this does not work, add write permissions
   to this file with 'chmod +x setup' and repeat
   step 2
5) Check the user guide at any time with
   'ctd --guide' or use 'ctd --help' for a 
   quick reference.

