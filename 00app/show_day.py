import setup, copy_list, util
import os
import subprocess

def print_day(context=0,
        edit=False,
        remove=False,
        only_show_char=False):

    util.set_context(context)
    day_path = util.get_list_path(context)

    if remove:
        if 'y' in raw_input('Are you sure you would like to delete the list file '+
                day_path+'?\n'):
            util.run('rm '+day_path)

    elif os.path.isfile(day_path):
        if edit:
            subprocess.call([
                setup.editor_command,day_path])
        with open(day_path,'r') as f:
            if only_show_char:
                for line in f.readlines():
                    if util.checkbox(only_show_char) in line:
                        print(line.strip())
            else:
                for line in f.readlines():
                    print(line.strip())

    else:

        # set prev day path to most recent existing list:
        source_offset = -1
        prev_day_path = util.get_list_path(context,source_offset)
        while not os.path.isfile(prev_day_path):
            if source_offset < -3651:
                prev_day_path = util.get_template_path('blank')
                break;
            else:
                source_offset -= 1
                prev_day_path = util.get_list_path(context,
                        source_offset)

        prev_day_str = "(N/A)"
        if(source_offset != -3652):
            prev_day_str = util.list_header(source_offset)[2:]+"\n"

        resp = str(raw_input('List not present. Create?\n'
                +"'y' = prev day, "+prev_day_str
                +"'i' = only incomplete tasks\n"
                +"'#' = # days before current context\n"
                +"'a' = abbrev template\n"
                +"'f' = full template\n"
                +"'e' = empty file\n"
                +"'n' = cancel\n"
                ))
        util.run('clear',verbose=True)

        if source_offset == -3652 and resp in 'yi':
            resp = 'e'

        if 'y' in resp:
            print("Creating "+day_path+" from "+prev_day_path)
            copy_list.copy_to_day(context,
                    source_path=prev_day_path)
            print_day(context,edit=True)

        elif 'i' in resp:
            print("Creating "+day_path+
                    " from incomplete items in "+
                    prev_day_path)
            copy_list.copy_to_day(context,
                    source_path=prev_day_path,
                    unmarked_only=True)
            print_day(context,edit=False)

        elif 'a' in resp:
            copy_list.copy_to_day(context,source_path
                    = setup.path_abbrev_template)
            print_day(context,edit=True)

        elif 'f' in resp:
            copy_list.copy_to_day(context,source_path
                    = setup.path_full_template)
            print_day(context,edit=True)

        elif 'e' in resp:
            util.overwrite_lines(day_path,
                util.list_header(context)+'\n')

        else:
            try:
                source_offset = int(resp)
                copy_list.copy_to_day(context,source_offset)
            except (ValueError, TypeError):
                return

