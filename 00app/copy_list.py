import subprocess,os
import util,setup

def copy_to_day(
        dest_offset,
        rel_source_offset=-1,
        source_path="",
        unmarked_only=False,
        most_recent=False):

    # source path not specified: use relative source offset instead.
    if len(source_path)==0:
        source_path = util.get_list_path(dest_offset,rel_source_offset)

    dest_path = util.get_list_path(dest_offset)

    # if source file exists, start by copying it, then modify.
    if os.path.isfile(source_path) and not os.path.isfile(dest_path):
        dest_list_header = (
                util.list_header(dest_offset))
        lines = []

        # read the file into lines
        lines = util.read_lines(source_path)

        # copy template and change start index if necessary
        start_index = 0 # start of post-template tasks
        if unmarked_only:
            if(setup.base_list_separator+'\n' in lines):
                start_index = len(lines)-lines[::-1].index(
                        setup.base_list_separator+'\n')
                lines = (lines[:start_index]+
                        [line for line in lines [start_index:]
                        if '[x]' not in line and not util.has_num_box(line)])
            else:
                print ('template format not present')

        lines = [uncheck_box(line) for line in
                [dest_list_header+'\n']+lines[1:]]

        util.overwrite_lines(dest_path,lines)

    else:
        print('Cannot copy '+source_path+'; file does not exist.')

def uncheck_box(line):
    sp1 = line.split('[',1)
    if (line[0] != '#' and
        len(sp1)==2 and len(sp1[1])>3
        and sp1[1][1]==']'):
        line = sp1[0]+'[ ]'+sp1[1][2:]
    return line
