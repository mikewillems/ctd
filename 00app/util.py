import setup
from subprocess import Popen, PIPE, call
from pathlib import Path as P
from datetime import date, timedelta


##################################
# DATETIME & FILE UTILS
##################################

def get_template_path(s):
    if s == 'blank':
        return setup.repo_location+'0.template.md'
    elif s == 'full':
        return setup.path_full_template
    elif 'abbr' in s:
        return setup.path_abbrev_template

def get_journal_template_path():
    journal_path = P(setup.journal_location+'journal.template.md')
    if not journal_path.exists():
        journal_path.mkdir()
    return str(journal_path)

today = date.today()

# Date format set here: changed from 'M.D.md' by default.
# See 'date' python module to make other formats,
# then change list_fname_format in setup.py.
def get_list_fname(context=0,offset=0):
    if type(context) == type(today):
        context = context.day-today.day
    day = today + timedelta(days=context+offset)
    return setup.list_fname_format.format(dt=day)

def get_journal_fname(offset=0):
    day = today + timedelta(days=offset)
    return setup.journal_fname_format.format(dt=day)

def get_list_path(context=0,offset=0):
    return setup.repo_location+get_list_fname(context,offset)

def get_journal_path(offset=0):
    return setup.journal_location+get_journal_fname(offset)

def get_day(days_offset=0):
    return today+timedelta(days=days_offset)


#################################
# SUMMARIZATION UTILS
#################################

# Returns true if has both numeral and checkbox,
# and is not commented / a markdown heading.
def has_task(s,unmarked=False):
    s0 = s.split('.',1)
    try:
        int(s0[0])
    except (IndexError, ValueError):
        return False
    if unmarked:
        try:
            if '[ ]' not in s0[1][:5]:
                return False
        except (IndexError, ValueError):
            return False
    else:
        try:
            s1 = s0[1][:5].split('[')
            if s1[1][1] != ']':
                return False
        except (IndexError, ValueError):
            return False
    return True

#################################
# FORMATTING & PARSING UTILS
#################################

checkbox = lambda char: ' ['+str(char)+'] '

task_line = (lambda task,num:
    str(num)+'. [ ] '+task)

list_header = (lambda offset: (setup.
        list_header_format.format(dt=get_day(offset)) + 
        (' Week '+get_day(offset).strftime('%U')
            if setup.include_week_in_list_header else '' ) + 
        (', ' + str(round(100.0 * get_day(offset).timetuple().tm_yday / 360, 1)) + '%'
            if setup.include_percent_in_list_header else '')
))

journal_header = (lambda offset: ('# Daily Journal\n## '+ get_day(offset).strftime('%a %Y.%m.%d') + 
                                  ' Week '+get_day(offset).strftime('%U')+ '\n\n'))

def has_num_box(line):
    try:
        return int(line.split('[')[1].split(']')[0])
    except (IndexError, ValueError):
        return False

# Parses a string into array of included numbers.
# Format can be: '1','1,3', '2-4', '2,3,7-9,5,13-17'
def str_extract_range(s,max_item):

    items = []

    try:
        s = int(s)
        if s < 0:
            s = str(s)
        else:
            return [s]
    except (ValueError, TypeError):
        s = str(s)
        if 'a' in s :
            return [i for i in
                    range(1,1+max_item)]

    groups = []

    try:
        groups = [sub.split('-') for sub in s.split(',')]
        for n in groups:
            if len(n)==1:
                t = int(n[0])
                items.append(int(n[0]))
            elif len(n)==2:
                if n[0]=='':
                    n[0]=1
                elif n[1]=='':
                    n[1]=max_item
                for t in range(int(n[0]),int(n[1])+1):
                    items.append(t)
            else:
                raise ValueError('invalid argument: '+str(n))
    except Exception as e:
        print('Problem parsing due to '+format(e))
        print('Empty range returned.')

    return list(set(items))


#################################
# FILE I/O AND STORAGE
#################################

def read_lines(filepath):
    with open(filepath,'r') as f:
        return f.readlines()

def overwrite_lines(filepath,lines):
    with open(filepath,'w') as f:
        f.write(''.join(lines))

def ensure_dir(path):
    run('mkdir -p '+path, working_dir=setup.bin_location)
    return path


############################
# STATE VARIABLES:
############################

# Stores the day offset most recently viewed:
def get_context():
    try:
        with open(setup.repo_location+'/.context','r') as f:
            contents = f.readlines()
            if len(contents) == 0:
                return 0
            return int(contents[0].strip())
    except IOError:
        return 0

def set_context(offset):
    context = offset
    with open(setup.repo_location+'/.context','w') as f:
        f.write(str(context))

context = get_context()


############################
# PROCESS UTILS:
############################


# Takes a single command or a list of
# separate commands and runs.
def run(command,
        working_dir=setup.repo_location,
        verbose=False):

    # If arg passed as 'ls' or 'ls -l':
    # Single command only
    if type(command) == type(''):
        command = [[s for s in str(command).
                split(' ') if len(s)>0]]

    # If arg passed as ['ls'], ['ls','-l']
    # Note: ['ls','cd'] will not work:
    # Single command only
    elif type(command[0]) == type(''):
        command = [command]

    # If arg passed as list of com lists:
    # [['ls','-l'],['cd','/home/...']], then
    # already in proper format.
    for c in command:
        if(verbose):
            run_verbose(c,working_dir)
        else:
            run_quiet(c,working_dir)

def run_verbose(com,loc):
    process = Popen(com,cwd=loc)
    process.wait()

def run_quiet(com,loc):
    process = Popen(com,
            cwd=loc,
            stdin=PIPE,
            stdout=PIPE)
    process.wait()

