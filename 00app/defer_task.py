import util, add_task, copy_list
import os
from collections import defaultdict

# mark tasks in current context as 'd', add them
# to context+rel_dest_offset
def defer(task_nums,rel_dest_offset):

    context = util.get_context()

    # Get lines from list file
    source_path = util.get_list_path(context)
    dest_path = util.get_list_path(
            context,rel_dest_offset)
    task_lines = []
    task_lines = util.read_lines(source_path)

    ########################################
    # Map each task # to corresponding line #
    # Define dictionary:
    # task_index[task_num]=[line_num,box,task]
    task_index = {}

        # ...by finding tasks in the lines
    for (line_num,line) in enumerate(task_lines):
        if '] ' in line[:9] and not '#' in line[:9]:
            spl1 = line.split('] ',1)
            spl2 = spl1[0].split('.',1)
            if len(spl1)+len(spl2)==4:
                task_num = spl2[0]
                # If a checkbox is commented, w/out num
                # would throw error - catch and move on
                try:
                    task_num = int(task_num)
                except ValueError:
                    continue
                box = spl2[1]+'] '
                # Finally, if all checks work, add to index.
                task = spl1[1]
                task_index[task_num]=[line_num,box,task]


    # Check if the following day exists
    if not os.path.isfile(dest_path):
        print ("Creating "+dest_path+
                " from full template...")
        copy_list.copy_to_day(context+
                rel_dest_offset,
                source_path =
                util.get_template_path('full'))

    # Mark specified checkboxes as deferred
    # and write to new day
    if 'r' in task_nums:
        todo = range(1,max(task_index)+1)
        todo = [i for i in todo if
                i in task_index.keys() and
                task_index[i][1] == ' [ ] ']
    else:
        todo = util.str_extract_range(task_nums,max(task_index))
    for task_num in todo:
        task_det = task_index[task_num]

        # Overwrite line in list:
        if not 'd' in task_det[1]:
            task_lines[task_det[0]] = (
                    str(task_num)+'.'+
                    util.checkbox('d')+
                    task_det[2])

            add_task.add(
                    task_det[2].strip().split(),
                    offset=rel_dest_offset+
                    context)

    # Write lines back to file:
    print task_lines
    util.overwrite_lines(source_path,task_lines)

