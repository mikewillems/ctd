import util

# Renumbers the tasks to not skip any.
def renumber_list(offset=0):

    # Get lines from list file
    path_to_day = util.get_list_path(util.today,offset)
    task_lines = util.read_lines(path_to_day)

    ########################################
    # Map each line # to corresponding task # and info
    # unless no task, then task_num = -1
    # line_index[line_num]=[task_num,box,task]
    line_index = {}

    # ...by finding tasks in the lines
    for (line_num,line) in enumerate(task_lines):
        if util.has_task(line):
            line_index[line_num]=line.split('.',1)
        else:
            line_index[line_num]=[-1,line]

    # Replace numbers with proper ones:
    task_num = 0
    for line_num in line_index.keys():
        if line_index[line_num][0]!=-1:
            task_num+=1
            task_lines[line_num]=(
                    str(task_num)+'.'+
                    line_index[line_num][1])

    # Write lines back to file:
    util.overwrite_lines(path_to_day,task_lines)
