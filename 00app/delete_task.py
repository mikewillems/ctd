import util
from collections import defaultdict



# delete list of tasks from specified day.
# returns deleted lines for verbosity
def delete(to_delete,offset=0):

    # Get lines from list file

    path_to_day = util.get_list_path(util.today,offset)
    task_lines = []
    task_lines = util.read_lines(path_to_day)

    # Map each task # to corresponding line #
    task_index = {}

        # ...by finding tasks in the lines
    for (line_num,line) in enumerate(task_lines):
        if ('] ' in line[:9] and
                not '#' in line[:9]):
            spl1 = line.split('] ',1)
            spl2 = spl1[0].split('.',1)
            if len(spl1)+len(spl2)==4:
                task_num = spl2[0]
        # If a checkbox is commented, w/out num
        # would throw error - catch and move on
                try:
                    task_num = int(task_num)
                except ValueError:
                    continue
                box = spl2[1]+'] '
        # Finally, if all checks work,
        # add to index.
                task = spl1[1]
                task_index[task_num]=[line_num,
                        box,task]

    # Delete lines specified:
    removed_lines = []
    for task_num in reversed(util.
            str_extract_range(
            to_delete,max(task_index.keys()))):

        # Check if task in list:
        if not task_num in task_index.keys():
            print ('task #'+str(task_num)+
                    ' not in list')
            continue

        # Remove line in list:
        removed_lines.append(
                task_lines.pop(
                task_index[task_num][0]).strip())

    # Write lines back to file:
    util.overwrite_lines(path_to_day,task_lines)

    # Return removed lines for notification:
    removed_lines.reverse()
    return removed_lines
