import setup, util
import subprocess
import os
from datetime import date, timedelta

today = date.today()

def eval_line(line):
    segments = line.split('<eval>')
    if len(segments) == 1:
        return line
    else:
        for i in range(1,len(segments)):
            sub_segments = segments[i].split('</eval>')
            # assumed that each segment has exactly one eval closing tag
            # which may or may not have additional text after it before
            # the next eval opening tag; thus there are either 1 or 2 sub_segments.
            result = str(eval(sub_segments[0]))
            if len(sub_segments) > 1:
                result += sub_segments[1]
            segments[i] = result
        return ''.join(segments)

def open_journal(offset=0,
        view=False,
        template=False,
        remove=False,
        list=0,
        ):

    if list:
        directory = sorted(os.listdir(setup.journal_location), reverse=True)
        entry = 0
        entries_shown = len(directory) if list > len(directory) else list
        print('Showing most recent '+str(entries_shown)+' of ' + str(len(directory)) + ' journal entries:')
        for fname in directory:
            if 'j' in fname and 'template' not in fname:
                entry += 1
                print(fname)
                if entry >= list:
                    break
        return

    journal_path = util.get_journal_path(offset)

    if view:
        if os.path.isfile(journal_path):
            with open(journal_path,'r') as f:
                for line in f.readlines():
                    print(line.strip())
        else:
            print('No journal entry found at '+ journal_path+'.')

    elif remove and 'y' in raw_input('Are you sure you would like to delete the daily journal: '+
                journal_path+'?\n'):
        util.run('rm '+journal_path)

    elif template:
        if not os.path.isfile(setup.path_journal_template):
            print('No journal template found at '+setup.path_journal_template+', creating...')
            if not os.path.exists(setup.journal_location):
                print("The specified journal location doesn't exist yet, attempting to create it now at "+setup.journal_location+"...")
                if subprocess.call(["mkdir","-p",setup.journal_location]) == 1:
                    print('''Cannot create journal location. Inadequate permissions or
                          file exists at that location. Please remove the file or adjust
                          in setup.py.''')
                    return
        subprocess.call([setup.editor_command, setup.path_journal_template])

    else:
        if not os.path.isfile(journal_path):
            new_journal_header = (util.journal_header(offset))
            lines = [new_journal_header] + util.read_lines(setup.path_journal_template)
            for i in range(len(lines)):
                try:
                    lines[i] = eval_line(lines[i])
                except Exception as e:
                    print('Problem evaluating line: '+line)
                    print('Error: '+str(e))
                    print('Line ignored.')
                    lines.remove(line)
                    continue
            util.overwrite_lines(journal_path,lines)
        subprocess.call([setup.editor_command,journal_path])